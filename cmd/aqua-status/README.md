# aqua-status

collection of endpoints to expose otherwise hard-to-reach numbers

something like this reverse proxy is necessary because because admin_ methods are not safe to expose to the internet.

the richlist and supply commands are behind the "admin_" namespace (instead of aqua_richlist or aqua_supply) because they could possibly be heavy on resources (this http server uses a 30 second cache mechanism to avoid constantly calling the rpc with same params).


## endpoints

```
	case "", "/", "/status":
		h.HomePage(w, r)
	case "/status/miners":
		h.StatusMiners(w, r)
	case "/status/versions", "/status/peers":
		h.StatusVersions(w, r)
	case "/richlist.txt":
		h.Richlist(w, r)
	case "/supply":
		h.Supply(w, r)
	case "/supply.wei":
```