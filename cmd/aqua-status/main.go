package main

import (
	"context"
	"flag"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/aerth/tgun"
	"gitlab.com/aquachain/aquachain/opt/aquaclient"
	rpc "gitlab.com/aquachain/aquachain/rpc/rpcclient"
)

const TimeoutDuration = 10 * time.Second

func NewHandler(aquaIPC string) *Handler {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*3)
	defer cancel()
	rpcc, err := rpc.DialIPC(ctx, aquaIPC)
	if err != nil {
		log.Fatal(err)
	}
	client := aquaclient.NewClient(rpcc)
	t := &tgun.Client{Timeout: TimeoutDuration}

	return &Handler{rpcc, client, map[string]*Cache{}, t, aquaIPC}
}

type Handler struct {
	Rpc        *rpc.Client
	Client     *aquaclient.Client
	cache      map[string]*Cache
	httpclient *tgun.Client
	rpcurl     string
}

func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println(r.Method, r.URL.Path, r.RemoteAddr, r.UserAgent())
	if r.Method != http.MethodGet {
		return
	}
	switch strings.TrimSuffix(r.URL.Path, "/") {
	case "", "/", "/status":
		h.HomePage(w, r)
	case "/status/miners":
		h.StatusMiners(w, r, false)
	case "/status/miners.json":
		h.StatusMiners(w, r, true)
	case "/status/versions.json", "/status/peers.json":
		h.StatusVersions(w, r, true)
	case "/status/versions", "/status/peers":
		h.StatusVersions(w, r, false)
	case "/richlist.txt":
		h.Richlist(w, r, false)
	case "/richlist.json":
		h.Richlist(w, r, true)
	case "/supply.json":
		h.Supply(w, r, true)
	case "/supply":
		h.Supply(w, r, false)
	case "/supply.wei":
		h.SupplyWei(w, r)
	default:
		http.NotFound(w, r)
	}
}

func main() {
	var (
		addrFlag = flag.String("addr", "127.0.0.1:8881", "address to listen")
		rpcFlag  = flag.String("rpc", os.ExpandEnv("$HOME/.aquachain/aquachain.ipc"), "path to aqua **ipc socket file**, should be owned by same system user")
	)
	flag.Parse()
	handler := NewHandler(*rpcFlag)
	err := http.ListenAndServe(*addrFlag, handler)
	if err != nil {
		log.Fatal(err)
	}
}
