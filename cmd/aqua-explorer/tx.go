package main

import (
	"context"
	"log"
	"math/big"
	"net/http"

	"gitlab.com/aquachain/aquachain/common"
	"gitlab.com/aquachain/aquachain/core/types"
	"gitlab.com/aquachain/aquachain/params"
)

func (a *aquaProx) displayTx(w http.ResponseWriter, r *http.Request, key string) {
	ctx := context.Background()
	tx, isPending, err := a.aquaclient.TransactionByHash(ctx, common.HexToHash(key))
	if err != nil {
		log.Println(err)
		return
	}
	w.Header().Set("content-type", "application/json")

	tx2, err := tx.AsMessage(types.NewEIP155Signer(params.MainnetChainConfig.ChainId))
	if err != nil {
		log.Println(err)
		return
	}

	receipt, err := a.aquaclient.TransactionReceipt(ctx, common.HexToHash(key))
	if err != nil {
		log.Printf("getting rceipt: %v", err)
		// reciept nil
	}

	from := tx2.From().Hex()
	fee := new(big.Int)
	fee.Sub(tx.Cost(), tx.Value())
	jsonNewEncoder(w).Encode([]interface{}{tx, map[string]interface{}{
		"isPending": isPending,
		"txFee":     InAqua(fee),
		"txTotal":   InAqua(tx.Cost()),
		"txValue":   InAqua(tx.Value()),
		"from":      from,
		"receipt":   receipt,
	}})

}
