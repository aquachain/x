package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"math/big"
	"net/http"
	"os"
	"strings"
	"sync/atomic"

	"github.com/aerth/tgun"
	"gitlab.com/aerth/simpledb"
	"gitlab.com/aquachain/aquachain/common"
	"gitlab.com/aquachain/aquachain/common/hexutil"
	"gitlab.com/aquachain/aquachain/core/types"
	aquaclient "gitlab.com/aquachain/aquachain/opt/aquaclient"
	"gitlab.com/aquachain/aquachain/params"
	rpcclient "gitlab.com/aquachain/aquachain/rpc/rpcclient"
)

func jsonNewEncoder(w io.Writer) *json.Encoder {
	enc := json.NewEncoder(w)
	enc.SetIndent(" ", " ")
	return enc
}

type aquaProx struct {
	nodeURL    string
	rpcclient  *rpcclient.Client
	aquaclient *aquaclient.Client
	db         *simpledb.SimpleDB
	head       *atomic.Value
	quitchan   chan os.Signal
	quit       bool
}

func main() {
	var (
		addr   = flag.String("addr", "127.0.0.1:8540", "")
		node   = flag.String("aqua", "http://127.0.0.1:8543", "path to aqua node")
		dbpath = flag.String("db", "aqua-indexed.db", "path to index db file")
		prox   = flag.String("prox", "", "")
	)
	flag.Parse()
	log.SetFlags(0)
	srv := &http.Server{}
	srv.Addr = *addr
	httpclient, err := (&tgun.Client{
		Proxy: *prox,
	}).HTTPClient()
	if err != nil {
		log.Fatalln(err)
	}

	db, err := simpledb.Load(*dbpath)
	if err != nil {
		log.Fatalln(err)
	}

	rpcc, err := rpcclient.DialHTTPWithClient(*node, httpclient)
	if err != nil {
		log.Fatalln(err)
	}
	ac := aquaclient.NewClient(rpcc)

	aprox := &aquaProx{
		nodeURL:    *node,
		aquaclient: ac,
		rpcclient:  rpcc,
		db:         db,
		head:       new(atomic.Value),
		quitchan:   make(chan os.Signal, 8),
	}
	srv.Handler = aprox
	// signal.Notify(aprox.quitchan, os.Interrupt, syscall.SIGTERM)
	if err := srv.ListenAndServe(); err != nil {
		log.Fatalln(err)
	}
}

func (a *aquaProx) serveFoo(w http.ResponseWriter, r *http.Request) {
	spl := strings.Split(r.URL.Path, "/")
	if (len(spl)) < 2 {

		http.NotFound(w, r)
		return
	}

	var latest uint64
	if err := a.db.Unmarshal("meta", "latest", &latest); err != nil {
		log.Println(err)
		http.NotFound(w, r)
		return
	}
	fmt.Fprintf(w, "%d", latest)
	// http.ResponseWriter, r *http.Request

}
func (a *aquaProx) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
	default:
		http.Error(w, "bad method", http.StatusMethodNotAllowed)
		return
	}
	var path1, path2 string
	spl := strings.Split(r.URL.Path, "/")
	if len(spl) != 2 && len(spl) != 3 {
		http.Error(w, "not found!", 404)
		return
	}
	path1 = spl[1]
	if len(spl) == 3 {
		path2 = spl[2]
	}
	switch path1 {
	case "foo":
		a.serveFoo(w, r)
		return
	case "head":
		head := a.head.Load().(*types.Header)
		log.Printf("/head: %s", head)
		jsonNewEncoder(w).Encode(head)
		return
	case "address", "addr":
		a.displayAddress(w, r, path2)
		return
	case "tx":
		a.displayTx(w, r, path2)
		return
	case "block":
		a.displayBlock(w, r, path2)
		return
	case "contracts":
		var contractList []string
		a.db.Unmarshal("contracts", "list", &contractList)
		jsonNewEncoder(w).Encode(map[string]interface{}{"number": len(contractList), "contracts": contractList})
		return
	case "contract":
		a.displayContract(w, r, path2)
		return
	default:
		http.NotFound(w, r)
		return

	}
}

type addressResponse struct {
	ConfirmedBalance string   `json:"balance"`
	PendingBalance   string   `json:"pendingBalance"`
	Nonce            uint64   `json:"nonce"`
	IncomingTx       []string `json:"incomingTx,omitempty"`
	OutgoingTx       []string `json:"outgoingTx,omitempty"`
	PendingNonce     uint64   `json:"pendingNonce,omitempty"`
}

func InAqua(b *big.Int) string {
	f := new(big.Float).SetInt(b)
	return f.Quo(f, new(big.Float).SetFloat64(params.Aqua)).String()
}

type Header struct {
	ParentHash   *common.Hash      `json:"parentHash"       gencodec:"required"`
	UncleHash    *common.Hash      `json:"sha3Uncles"       gencodec:"required"`
	Coinbase     *common.Address   `json:"miner"            gencodec:"required"`
	Root         *common.Hash      `json:"stateRoot"        gencodec:"required"`
	TxHash       *common.Hash      `json:"transactionsRoot" gencodec:"required"`
	ReceiptHash  *common.Hash      `json:"receiptsRoot"     gencodec:"required"`
	Bloom        *types.Bloom      `json:"logsBloom"        gencodec:"required"`
	Difficulty   *hexutil.Big      `json:"difficulty"       gencodec:"required"`
	Number       *hexutil.Big      `json:"number"           gencodec:"required"`
	GasLimit     *hexutil.Uint64   `json:"gasLimit"         gencodec:"required"`
	GasUsed      *hexutil.Uint64   `json:"gasUsed"          gencodec:"required"`
	Time         *hexutil.Big      `json:"timestamp"        gencodec:"required"`
	Extra        *hexutil.Bytes    `json:"extraData"        gencodec:"required"`
	MixDigest    *common.Hash      `json:"mixHash"          gencodec:"required"`
	Nonce        *types.BlockNonce `json:"nonce"            gencodec:"required"`
	Version      byte              `json:"version" gencodec:"required"`
	Transactions []string          `json:"transactions"`
}

func (a *aquaProx) displayContract(w http.ResponseWriter, r *http.Request, key string) {}
