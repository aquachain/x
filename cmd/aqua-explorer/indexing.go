package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/big"
	"time"

	"gitlab.com/aerth/simpledb"
	"gitlab.com/aquachain/aquachain/common"
	"gitlab.com/aquachain/aquachain/common/hexutil"
	"gitlab.com/aquachain/aquachain/core/types"
	"gitlab.com/aquachain/aquachain/params"
)

type JsonBlock struct {
	Transactions []*types.Transaction `json:"transactions"`
	Uncles       []string             `json:"uncles"`
}
type AJsonBlock struct {
	ParentHash   *common.Hash         `json:"parentHash"       gencodec:"required"`
	UncleHash    *common.Hash         `json:"sha3Uncles"       gencodec:"required"`
	Coinbase     *common.Address      `json:"miner"            gencodec:"required"`
	Root         *common.Hash         `json:"stateRoot"        gencodec:"required"`
	TxHash       *common.Hash         `json:"transactionsRoot" gencodec:"required"`
	ReceiptHash  *common.Hash         `json:"receiptsRoot"     gencodec:"required"`
	Bloom        *types.Bloom         `json:"logsBloom"        gencodec:"required"`
	Difficulty   *hexutil.Big         `json:"difficulty"       gencodec:"required"`
	Number       *hexutil.Big         `json:"number"           gencodec:"required"`
	GasLimit     *hexutil.Uint64      `json:"gasLimit"         gencodec:"required"`
	GasUsed      *hexutil.Uint64      `json:"gasUsed"          gencodec:"required"`
	Time         *hexutil.Big         `json:"timestamp"        gencodec:"required"`
	Extra        *hexutil.Bytes       `json:"extraData"        gencodec:"required"`
	MixDigest    *common.Hash         `json:"mixHash"          gencodec:"required"`
	Nonce        *types.BlockNonce    `json:"nonce"            gencodec:"required"`
	Transactions []*types.Transaction `json:"transactions,omitempty"`
}

func (a *aquaProx) startIndexing() error {
	if true {
		return fmt.Errorf("broken")
	}
	if a.quit {

		log.Println("already quit")
		return nil
	}
	var latest uint64
	// get latest indexed, or zero
	if err := a.db.Unmarshal("meta", "latest", &latest); err != nil {
		if err != simpledb.ErrNotFound {
			log.Println(err)
			return err
		} else {
			log.Println("creating new index")
			a.db.Marshal("meta", "latest", 0)
		}
	}
	// get head
	{
		headblock, err := a.aquaclient.BlockByNumber(context.Background(), nil)
		if err != nil {
			log.Fatal("cant get head", err)
		}
		headblock.SetVersionConfig(params.MainnetChainConfig)
		log.Println("head block:", headblock)
		a.head.Store(headblock.Header())
	}

	// get latest header
	time.Sleep(time.Second)
	bn := new(big.Int)
	for i := latest; i < a.head.Load().(*types.Header).Number.Uint64(); i++ {
		var resp JsonBlock
		log.Printf("loading block %d", i)
		bn.SetUint64(i)
		var respstr json.RawMessage

		err := a.rpcclient.Call(&respstr, "aqua_getBlockByNumber", "0x"+bn.Text(16), true)
		if err != nil {
			log.Fatalln("making first latest:", err)
			return err
		}
		if err := json.Unmarshal(respstr, &resp); err != nil {
			log.Fatalln(err)
		}

		txs := resp.Transactions
		if len(txs) != 0 {
			log.Printf("Block %d has %d transactions", i, len(txs))
		}
		for txi, tx := range txs {
			_ = txi
			a.indexTx(i, tx)
		}

		a.db.Marshal("meta", "latest", i)

		select {
		case sig := <-a.quitchan:
			log.Println("Done now", sig.String())
			a.quit = true
			return nil
		default:

			log.Println("continung")
		}
		if a.quit {
			log.Println("quitting")
			return nil
		}

	}
	log.Println("Sleep 10")
	<-time.After(time.Second * 10)
	log.Println("Relooping!")

	return a.startIndexing()
}

func (a *aquaProx) indexTx(number uint64, tx *types.Transaction) {
	tx2, err := tx.AsMessage(types.NewEIP155Signer(params.MainnetChainConfig.ChainId))
	if err != nil {
		log.Println(err)
		return
	}
	from := tx2.From().String()
	txhash := tx.Hash().String()
	desti := common.Address{}.String() // zero addr
	if to := tx.To(); to != nil {
		desti = tx.To().String()
	} else {

		ctx := context.Background()

		receipt, err := a.aquaclient.TransactionReceipt(ctx, tx.Hash())
		if err != nil {
			log.Printf("getting rceipt: %v", err)
			// reciept nil
		}

		contract := receipt.ContractAddress
		log.Println("new contract:", contract.String())
		var contractList []string
		a.db.Unmarshal("contracts", "list", &contractList)
		if contract != (common.Address{}) && (len(contractList) == 0 || contractList[len(contractList)-1] != contract.String()) {
			contractList = append(contractList, contract.String())
			a.db.Marshal("contracts", "list", contractList)
		}

	}

	log.Println("indexing tx:", txhash)
	incomingTx := []string{}
	outgoingTx := []string{}
	a.db.Unmarshal("incoming", desti, &incomingTx)
	a.db.Unmarshal("outgoing", from, &outgoingTx)
	if len(incomingTx) == 0 || incomingTx[len(incomingTx)-1] != txhash {
		//	log.Printf("%s incoming %d=>%d", from, len(incomingTx), len(incomingTx)+1)
		//	log.Printf("%s outgoing %d=>%d", desti, len(outgoingTx), len(outgoingTx)+1)
		//	log.Println("Adding incoming tx", txhash, "destination", desti, "total", incomingTx)
		incomingTx = append(incomingTx, txhash)
	}
	if len(outgoingTx) == 0 || outgoingTx[len(outgoingTx)-1] != txhash {
		//	log.Println("Adding Outgoing tx", txhash, "destination", desti, "total", outgoingTx)
		outgoingTx = append(outgoingTx, txhash)
	}
	log.Println("# outgoingtx", from, len(outgoingTx))
	log.Println("# incomingtx", desti, len(incomingTx))

	a.db.Marshal("incoming", desti, incomingTx)
	a.db.Marshal("outgoing", from, outgoingTx)

}
