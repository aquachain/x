PWD != pwd
BUILD_DIR ?= ${PWD}/bin
GO_PACKAGES ?= ./cmd/...
CGO_ENABLED := 0
export CGO_ENABLED
.PHONY: ${BUILD_DIR}
gopkgs = $(shell go list ${GO_PACKAGES})
default: lint golang-ci
	@echo linter passed. use 'make all' to build binaries
lint:
	CGO_ENABLED=${CGO_ENABLED} go vet ./...

all: ${BUILD_DIR}

explorer: ${BUILD_DIR}/aqua-explorer
${BUILD_DIR}/aqua-explorer: cmd/aqua-explorer/main.go
	CGO_ENABLED=${CGO_ENABLED} GOBIN=${BUILD_DIR} go install -v "${GO_PACKAGES}" ./cmd/aqua-explorer

golang-ci:
	golangci-lint run

${BUILD_DIR}: */*.go */*/*.go
	CGO_ENABLED=${CGO_ENABLED} GOBIN=${BUILD_DIR} go install -v "${GO_PACKAGES}"

$(gopkgs):
	CGO_ENABLED=${CGO_ENABLED} go get -v -d -t ./...

clean:
	${RM} -rf build/bin
	${RM} cmd/aqua-status/aqua-status cmd/aqua-puppet/aqua-puppet cmd/aquahexer/aquahexer \
	cmd/aquap2psim/aquap2psim cmd/supplyproxy/supplyproxy cmd/aqua-explorer/aqua-explorer \
	cmd/explorer/explorer cmd/aqua-wnode/aqua-wnode cmd/aqua-vm/aqua-vm \
	cmd/aqua-rlpdump/aqua-rlpdump cmd/ahex/ahex cmd/aquacli/aquacli \
	cmd/aqua-abigen/aqua-abigen 