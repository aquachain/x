module gitlab.com/aquachain/x

go 1.18

require (
	github.com/aerth/tgun v0.1.6
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-colorable v0.1.12
	github.com/olekukonko/tablewriter v0.0.4
	github.com/wcharczuk/go-chart v2.0.1+incompatible
	gitlab.com/aerth/simpledb v0.0.3
	gitlab.com/aquachain/aquachain v1.7.14
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d
	gopkg.in/urfave/cli.v1 v1.20.0
)

require (
	github.com/blend/go-sdk v1.20210221.5 // indirect
	github.com/btcsuite/btcd v0.21.0-beta // indirect
	github.com/cpuguy83/go-md2man/v2 v2.0.2 // indirect
	github.com/deckarep/golang-set v1.8.0 // indirect
	github.com/edsrzf/mmap-go v1.1.0 // indirect
	github.com/fatih/color v1.13.0 // indirect
	github.com/go-stack/stack v1.8.1 // indirect
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/golang/snappy v0.0.4 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/hashicorp/golang-lru v0.5.4 // indirect
	github.com/huin/goupnp v1.0.3 // indirect
	github.com/jackpal/go-nat-pmp v1.0.2 // indirect
	github.com/julienschmidt/httprouter v1.3.0 // indirect
	github.com/karalabe/hid v1.0.0 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/pborman/uuid v1.2.1 // indirect
	github.com/peterh/liner v1.2.2 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/robertkrimen/otto v0.0.0-20211024170158-b87d35c0b86f // indirect
	github.com/rs/cors v1.8.2 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	github.com/urfave/cli v1.22.9 // indirect
	go.etcd.io/bbolt v1.3.5 // indirect
	golang.org/x/image v0.0.0-20201208152932-35266b937fa6 // indirect
	golang.org/x/mod v0.6.0-dev.0.20220419223038-86c51ed26bb4 // indirect
	golang.org/x/net v0.0.0-20220624214902-1bab6f366d9e // indirect
	golang.org/x/sync v0.0.0-20220601150217-0de741cfad7f // indirect
	golang.org/x/sys v0.0.0-20220624220833-87e55d714810 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/tools v0.1.11 // indirect
	google.golang.org/protobuf v1.28.0 // indirect
	gopkg.in/natefinch/npipe.v2 v2.0.0-20160621034901-c1b8fa8bdcce // indirect
	gopkg.in/olebedev/go-duktape.v3 v3.0.0-20210326210528-650f7c854440 // indirect
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
)

replace gitlab.com/aquachain/aquachain => ../aquachain/
