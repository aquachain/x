
# aqua-explorer (json)

```
# run server (change to point to your aqua node)
./bin/aqua-explorer   -aqua https://c.onical.org
# another terminal...
curl localhost:8540/block/1
```

## endpoints

/tx/

/block/

/address/


## indexing is experimental and disabled atm.

## WIP: indexed transactions

now takes a Long Time to index, and may be busy during synced normal operation, we'll see

add `?full=true` to URL to expose incomingTx and outgoingTx array in response

example: `/address/0x000...0000?full=true`


## WIP: list contracts

/contracts


