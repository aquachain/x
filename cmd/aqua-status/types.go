package main

type PoolStats struct {
	CandidatesTotal int     `json:"candidatesTotal"`
	Hashrate        int     `json:"hashrate"`
	ImmatureTotal   int     `json:"immatureTotal"`
	MaturedTotal    int     `json:"maturedTotal"`
	MinersTotal     int     `json:"minersTotal"`
	Nodes           []Nodes `json:"nodes"`
	Now             int64   `json:"now"`
	Stats           Stats   `json:"stats"`
}
type Nodes struct {
	Difficulty string `json:"difficulty"`
	Height     string `json:"height"`
	LastBeat   string `json:"lastBeat"`
	Name       string `json:"name"`
}
type Stats struct {
	LastBlockFound int64 `json:"lastBlockFound"`
	RoundShares    int64 `json:"roundShares"`
}
