package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"regexp"

	"gitlab.com/aquachain/aquachain/common"
)

func validateAddress(s string) bool {
	return regAddress.MatchString(s) && common.HexToAddress(s) != common.Address{0}
}

var regAddress = regexp.MustCompile("^0x[0-9a-fA-F]{40}$")

func (a *aquaProx) displayAddress(w http.ResponseWriter, r *http.Request, key string) {
	if key == "" {
		fmt.Fprintf(w, "/address/<address>\n")
		return
	}
	if !validateAddress(key) {
		fmt.Fprintf(w, "invalid address\n")
		return
	}

	addr := common.HexToAddress(key)
	ctx := context.Background()
	bal, err := a.aquaclient.Balance(ctx, addr)
	if err != nil {
		log.Println("error fetching balance for", addr.String(), err)
		http.Error(w, "error.", 500)
		return
	}

	pbal, err := a.aquaclient.PendingBalance(ctx, addr)
	if err != nil {
		log.Println("error fetching pbalance for", addr.String(), err)
		http.Error(w, "error.", 500)
		return
	}
	pnonce, err := a.aquaclient.PendingNonce(ctx, addr)
	if err != nil {
		log.Println("error fetching pnonce for", addr.String(), err)
		http.Error(w, "error.", 500)
		return
	}
	nonce, err := a.aquaclient.NonceAt(ctx, addr, nil)
	if err != nil {
		log.Println("error fetching pnonce for", addr.String(), err)
		http.Error(w, "error.", 500)
		return
	}
	if nonce == pnonce {
		pnonce = 0 // omitempty
	}

	w.Header().Set("content-type", "application/json")
	showFull := r.URL.Query().Get("full") != ""
	if !showFull {
		jsonNewEncoder(w).Encode(addressResponse{
			PendingBalance:   InAqua(pbal),
			ConfirmedBalance: InAqua(bal),
			Nonce:            nonce,
			PendingNonce:     pnonce,
		})
		return
	}
	incomingTx := []string{}
	a.db.Unmarshal("incoming", addr.String(), &incomingTx)

	outgoingTx := []string{}
	a.db.Unmarshal("outgoing", addr.String(), &outgoingTx)
	jsonNewEncoder(w).Encode(addressResponse{
		PendingBalance:   InAqua(pbal),
		ConfirmedBalance: InAqua(bal),
		Nonce:            nonce,
		PendingNonce:     pnonce,
		IncomingTx:       incomingTx,
		OutgoingTx:       outgoingTx,
	})
}
