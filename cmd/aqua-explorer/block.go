package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"gitlab.com/aquachain/aquachain/common/hexutil"
	"gitlab.com/aquachain/aquachain/params"
)

func (a *aquaProx) displayBlock(w http.ResponseWriter, r *http.Request, key string) {
	if key == "" {
		fmt.Fprintf(w, "/block/<block>\n")
		return
	}
	//	var block = types.NewBlockWithHeader(new(types.Header))
	var block = new(Header)
	method := "aqua_getBlockByNumber"
	if strings.HasPrefix(key, "0x") {
		method = "aqua_getBlockByHash"
	}
	num, err := strconv.ParseUint(key, 10, 64)
	if err == nil {
		key = hexutil.EncodeUint64(num)
	}
	err = a.rpcclient.Call(&block, method, key, false)
	if err != nil {
		log.Println(err)
		return
	}
	if block == nil {
		http.Error(w, "{}", 404)
		return
	}
	block.Version = byte(params.MainnetChainConfig.GetBlockVersion(block.Number.ToInt()))
	w.Header().Set("content-type", "application/json")
	jsonNewEncoder(w).Encode([]interface{}{block})
	return
}
